package org.opentele.server.provider.integration.archive

import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import org.opentele.server.provider.ClinicianService

@TestFor(PhmrGenerationService)
@TestMixin(GrailsUnitTestMixin)
class PhmrGenerationServiceTest {

  def phmrGenerationService

  void setUp() {
    phmrGenerationService = new PhmrGenerationService()
  }

  void testBuild() {

  }

  void testCpr2Birthdate() {
    // invalid cpr length
    def a = phmrGenerationService.cpr2Birthdate("12345")
    assertNull(a)
    // defaults to 1900
    def b = phmrGenerationService.cpr2Birthdate("251248")
    assertEquals([1948,11,25], b)
    // index 4 or 9 and y > 36
    def c = phmrGenerationService.cpr2Birthdate("251248-4916")
    assertEquals([1948,11,25], c)
    // index 4 or 9 and y <= 36
    def d = phmrGenerationService.cpr2Birthdate("251224-9916")
    assertEquals([2024,11,25], d)
    // index between 5 and 8 and y <= 36
    def e = phmrGenerationService.cpr2Birthdate("251224-5916")
    assertEquals([2024,11,25], e)
    // index between 5 and 8 and y >= 58
    def f = phmrGenerationService.cpr2Birthdate("251260-6916")
    assertEquals([1860,11,25], f)
    // index between 5 and 8 and y otherwise
    def g = phmrGenerationService.cpr2Birthdate("251240-7916")
    assertEquals([1940,11,25], g)
    // index otherwise
    def h = phmrGenerationService.cpr2Birthdate("251248-3916")
    assertEquals([1948,11,25], h)
    // without dash
    def i = phmrGenerationService.cpr2Birthdate("2512484916")
    assertEquals([1948,11,25], i)
  }
}
