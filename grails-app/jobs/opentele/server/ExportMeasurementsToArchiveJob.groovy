package opentele.server

class ExportMeasurementsToArchiveJob {

    def archiveExportService

    def concurrent = false

    static triggers = { }

    def execute() {
        archiveExportService.exportToArchive()
    }
}

