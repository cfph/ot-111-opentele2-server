package org.opentele.server.provider.integration.archive

import org.net4care.xdsconnector.service.RegistryError
import org.opentele.server.model.Measurement

public class XdsWebServiceClientService {

  def phmrGenerationService
  def xdsRepositoryConnector

  boolean sendMeasurement(Measurement measurement) {
    def phmr, result
    try {
      // TODO: pass XML Document instead of string
      phmr = phmrGenerationService.build(measurement)
      result = xdsRepositoryConnector.provideAndRegisterCDADocument(phmr)
      if (result.status.endsWith("Success")) {
        // log.debug("Sent measurement to XDS (id:'${measurement.id}')")
        return true
      }
      else {
        def message = result.registryErrorList?.registryError?.collect {
          String.format("%s: %s %s", it.errorCode, it?.codeContext ?: "", it?.value ?: "")
        }.join(", ")
        log.error("Failed uploading measurement to XDS (id:'${measurement.id}'), message is: '${message}'")
      }
    } catch (Throwable t) {
      log.error("Error occured while trying to upload measurement to XDS (id:'${measurement.id}')", t)
    }
    return false
  }
}
