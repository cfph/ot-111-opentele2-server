package org.opentele.server.provider.integration.archive

import org.opentele.server.model.Measurement

class ArchiveMeasurementService {
    def measurementsToExport() {
        return Measurement.createCriteria().list {
            and {
                eq('exportedToArchive', false)
                measurementType {
                    not {
                        inList('name', Measurement.notToBeExportedMeasurementTypes)
                    }
                }
            }
        }
    }

    def markAsExported(Measurement measurement) {
        measurement.exportedToArchive = true
        measurement.save(failOnError: true)
    }

}