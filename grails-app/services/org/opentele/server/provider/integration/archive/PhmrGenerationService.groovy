package org.opentele.server.provider.integration.archive

import org.net4care.phmr.builders.DanishPHMRBuilder
import org.net4care.phmr.codes.DAK
import org.net4care.phmr.model.AddressData
import org.net4care.phmr.model.Context
import org.net4care.phmr.model.DanishPHMRModel
import org.net4care.phmr.model.MedicalEquipment
import org.net4care.phmr.model.Measurement as PhmrMeasurement
import org.net4care.phmr.model.OrganizationIdentity
import org.net4care.phmr.model.PersonIdentity
import org.net4care.phmr.model.SimpleClinicalDocument
import org.opentele.server.core.model.types.MeasurementTypeName
import org.opentele.server.core.model.types.Sex
import org.opentele.server.model.Clinician
import org.opentele.server.model.Measurement
import org.opentele.server.model.Measurement as OpenTeleMeasurement
import org.opentele.server.model.Patient
import org.w3c.dom.Document

public class PhmrGenerationService {

    def build(OpenTeleMeasurement measurement) {
        SimpleClinicalDocument cda = new DanishPHMRModel();

        Date now = new Date()
        Date acknowledgedDate = measurement.measurementNodeResult?.acknowledgedDate ?: now
        cda.effectiveTime = acknowledgedDate
        cda.setDocumentVersion("2358344", 1) // TODO: which version?

        cda.patient = buildPatient(measurement.patient)

        def clinician = getClinician(measurement)
        def phmrClinician = buildClinician(clinician)
        def phmrOrganization = buildOrganization(clinician)
        cda.custodian = phmrOrganization
        cda.setAuthor(phmrOrganization, phmrClinician, acknowledgedDate)
        cda.setAuthenticator(phmrOrganization, phmrClinician, acknowledgedDate)

        cda.setDocumentationTimeInterval(measurement.startTime ?: now, measurement.endTime ?: now)
        buildMeasurements(measurement).each { cda.addResult(it)}
        cda.addMedicalEquipment(buildEquipment(measurement))

        // construct PHMR
        def phmrBuilder = new DanishPHMRBuilder()
        cda.construct(phmrBuilder)
        return phmrBuilder.documentAsString
    }

    private Clinician getClinician(Measurement measurement) {
        // get the clinician acknowledging the measurement
        def clinician = measurement.measurementNodeResult?.completedQuestionnaire?.acknowledgedBy
        def source = "acknowledgedBy"
        if (!clinician) {
            // not acknowledged, use consultation
            clinician = measurement.consultation?.clinician
            source = "consultation"
        }
        if (!clinician && measurement.patient.conferences) {
            // no consultation, use conference
            clinician = measurement.patient.conferences.find{ it?.clinician }?.clinician
            source = "conferences"
        }
        if (!clinician) {
            // still no clinician, assign a dummy
            clinician = new Clinician(lastName: "N/A", firstName: "")
            source = "dummy"
        }
        log.debug("Using clinician: ${clinician} from ${source}")
        return clinician
    }

    private PersonIdentity buildPatient(Patient patient) {
        AddressData patientAddress = new AddressData.AddressBuilder(patient.postalCode, patient.city)
            .setCountry("Denmark") // TODO
            .addAddressLine(patient.address)
            .setUse(AddressData.Use.HomeAddress)
            .build()

        return new PersonIdentity.PersonBuilder(patient.lastName)
            .addGivenName(patient.firstName)
            .setGender(sex2Gender(patient.sex))
            .setPersonID(patient.cpr)
            .setAddress(patientAddress)
            .addTelecom(AddressData.Use.HomeAddress, "tel", patient.phone ?: "")
            // TODO .addTelecom(AddressData.Use.MobileContact, "tel", patient.mobilePhone ?: "")
            .setBirthTime(cpr2Birthdate(patient.cpr))
            .build()
    }

    private OrganizationIdentity buildOrganization(Clinician clinician) {
        AddressData clinicianAddress = new AddressData.AddressBuilder("", "") // TODO
            .setCountry("Denmark") // TODO
            .addAddressLine("") // TODO
            .setUse(AddressData.Use.WorkPlace)
            .build()

        return new OrganizationIdentity.OrganizationBuilder()
            .setName(clinician.name)
            .addTelecom(AddressData.Use.WorkPlace, "tel", clinician.phone ?: "")
            // TODO .addTelecom(AddressData.Use.MobileContact, "tel", patient.mobilePhone ?: "")
            .setAddress(clinicianAddress)
            .build()
    }

    private PersonIdentity buildClinician(Clinician clinician) {
        return new PersonIdentity.PersonBuilder(clinician.lastName)
            .addGivenName(clinician.firstName)
            // TODO .setPersonID(clinician.id)
            .addTelecom(AddressData.Use.WorkPlace, "tel", clinician.phone ?: "")
            // TODO .addTelecom(AddressData.Use.MobileContact, "tel", clinician.mobilePhone ?: "")
            .build()
    }

    private List<PhmrMeasurement> buildMeasurements(OpenTeleMeasurement measurement) {
        // TODO: OpenTele does not register how and where the value originated
        // measurement.measurementNodeResult.patientQuestionnaireNode.templateQuestionnaireNode
        // measurement.conference.measurementDrafts.find{???}.automatic
        Context context = new Context(Context.ProvisionMethod.TypedByCitizen, Context.PerformerType.Citizen);

        // TODO: externalize measurement units and display names
        if (measurement.measurementType.name.equals(MeasurementTypeName.WEIGHT)) {
            return [buildNpuMeasurement(measurement.value, measurement.time, "Kg", "NPU03804", "Legeme v�gt;Pt", context)]
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.PULSE)) {
            return [buildNpuMeasurement(measurement.value, measurement.time, "Slag/min", "NPU21692", "Puls;Hjerte", context)]
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.BLOOD_PRESSURE)) {
            return [buildNpuMeasurement(measurement.diastolic, measurement.time, "mmHg", "DNK05473", "Blodtryk diastolisk;Arm", context),
                buildNpuMeasurement(measurement.systolic, measurement.time, "mmHg", "DNK05472", "Blodtryk systolisk;Arm", context)]
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.BLOODSUGAR)) {
            // TODO: circumstance (after meal, before meal, or control measurement)
            return [buildNpuMeasurement(measurement.value, measurement.time, "mmol/L", "NPU02187", "Glukose;B", context)]
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.CRP)) {
            return [buildNpuMeasurement(measurement.value, measurement.time, "nmol/L", "NPU01423", "C-reaktivt protein [CRP];P", context)]
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.CTG)) {
            // TODO: not supported in KIH
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.HEMOGLOBIN)) {
            // TODO: not supported in KIH
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.LUNG_FUNCTION)) {
            return [buildDakMeasurement(measurement.value, measurement.time, "Liter/sekund", "MCS88015", "FEV1", context),
                buildDakMeasurement(measurement.fev6, measurement.time, "Liter/sekund", "MCS88016", "FVC", context)] // FVC ~ FEV6
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.SATURATION)) {
            return [buildNpuMeasurement(measurement.value, measurement.time, "%", "NPU03011", "O2 sat.;Hb(aB)", context)]
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.TEMPERATURE)) {
            return [buildNpuMeasurement(measurement.value, measurement.time, "�C", "NPU08676", "Legeme temp.;Pt", context)]
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.URINE)) {
            return [buildNpuMeasurement(measurement.protein.value(), measurement.time, "g/L", "NPU03958", "Protein;U", context)]
        } else if (measurement.measurementType.name.equals(MeasurementTypeName.URINE_GLUCOSE)) {
            return [buildNpuMeasurement(measurement.glucoseInUrine.value(), measurement.time, "mmol/L", "NPU03936", "Glukose;U", context)]
        }
        log.error("Unsupported measurement type ${measurement.measurementType.name} encountered.")
        return []
    }

    private MedicalEquipment buildEquipment(OpenTeleMeasurement measurement) {
        // TODO: medical equipment currently not part of KIH
        return new MedicalEquipment.MedicalEquipmentBuilder()
            // TODO: what about measurement.deviceIdentification
            .setMedicalDeviceDisplayName(measurement.meter?.model ?: "")
            .setMedicalDeviceCode(measurement.meter?.meterId ?: "")
            .setManufacturerModelName("")
            .setSoftwareName("")
            .build()
    }

    private buildNpuMeasurement(Double value, Date time, String unit, String code, String displayname, Context context) {
        return buildNpuMeasurement(value.toString(), time, unit, code, displayname, context)
    }

    private buildNpuMeasurement(String value, Date time, String unit, String code, String displayname, Context context) {
        return new PhmrMeasurement.MeasurementBuilder(time, PhmrMeasurement.Status.COMPLETED)
            .setPhysicalQuantity(value, unit, code, displayname)
            .setContext(context)
            .build()
    }

    private buildDakMeasurement(Double value, Date time, String unit, String code, String displayname, Context context) {
        return new PhmrMeasurement.MeasurementBuilder(time, PhmrMeasurement.Status.COMPLETED)
            .useAlternativeCodingSystem(DAK.CODESYSTEM_OID, DAK.DISPLAYNAME)
            .setPhysicalQuantity(value, unit, code, displayname)
            .setContext(context)
            .build()
    }

    // TODO: move to Net4Care PHMR Builder as setBirthTimeFromCpr
    protected cpr2Birthdate(String cpr) {
        if (cpr?.length() < 6) return null
        int d = cpr.substring(0,2) as int
        int m = cpr.substring(2,4) as int
        int y = cpr.substring(4,6) as int
        int n = 0 // defaults to 1900
        if (cpr.length() >= 10) {
            n = ((cpr[6] == "-") ? cpr[7] : cpr[6]) as int
        }
        int c
        if (n == 4 || n == 9)
            c = (y <= 36) ? 2000 : 1900
        else if (5 <= n && n <= 8)
            c = (y <= 36) ? 2000 : (y >= 58) ? 1800 : 1900
        else // if (0 < n && n <= 3)
            c = 1900
        return [c + y, m - 1, d]
    }

    private sex2Gender(Sex sex) {
        switch (sex) {
            case Sex.FEMALE:
                PersonIdentity.Gender.Female
            case Sex.MALE:
                PersonIdentity.Gender.Male
            default:
                PersonIdentity.Gender.Undifferentiated
        }
    }
}