package org.opentele.server.provider.integration.archive

import org.opentele.server.model.Measurement

class ArchiveExportService {

    def archiveService // instantiated in resources.groovy
    def grailsApplication
    def archiveMeasurementService

    def exportToArchive() {
        archiveMeasurementService.measurementsToExport().each { Measurement measurement ->
            try {
                log.debug("Exporting measurement (id:'${measurement.id}', type: ${measurement?.measurementType?.name}) to the archive")

                def measurementExported = archiveService.sendMeasurement(measurement)

                if (measurementExported) {
                    log.info("Measurement (id:'${measurement.id}') was exported to the archive")
                    archiveMeasurementService.markAsExported(measurement)
                }
            } catch (Exception ex) {
                log.info("Failed exporting Measurement (id:'${measurement.id}') to the archive!", ex)
            }
        }
    }
}
